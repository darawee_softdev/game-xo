
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Darawee
 */
public class XO {

    static String player = "X";
    static String table[][] = {
        {" ", "1", "2", "3"},
        {"1", "-", "-", "-"},
        {"2", "-", "-", "-"},
        {"3", "-", "-", "-"},};
    static int Round = 0;
    static Scanner kb = new Scanner(System.in);

    public static void main(String[] args) {
        printWelcome();
        for (;;) {
            printTable(table);
            inputPosition();
            if (checkWin()) {
                break;
            } else if (checkDraw()) {
                break;
            }
            switchPlayer();
        }
    }

    public static void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void printTable(String[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println("");
        }
    }

    public static void printPlayer() {
        System.out.println(player + " " + "turn");
        System.out.println("Please input Row Col : ");
    }

    public static void inputPosition() {      
        for (;;) {
            printPlayer();
            try {
                int R = kb.nextInt();
                int C = kb.nextInt();
                if (!(table[R][C].equals("-"))) {
                    System.out.println("Try again");
                    continue;
                }
                Round++;
                table[R][C] = player;
                printTable(table); break;                
            } catch (IndexOutOfBoundsException ex) {
                System.out.println("Row and Column must be 1-3");
                continue;
            }
        }

    }

    public static void switchPlayer() {
        if (player == "X") {
            player = "O";
        } else {
            player = "X";
        }
    }

    public static boolean checkDraw() {
        if (Round == 9) {
            System.out.println("Draw!!!");
            System.out.println("Bye Bye ... ");
            return true;
        }
        return false;
    }

    public static boolean checkCol() {
        for (int i = 0; i < table.length; i++) {
            if (table[1][i] == player && table[2][i] == player
                    && table[3][i] == player) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkRow() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][1] == player && table[i][2] == player
                    && table[i][3] == player) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkCross() {
        if (table[1][1] == player && table[2][2] == player
                && table[3][3] == player) {
            return true;
        }
        if (table[1][3] == player && table[2][2] == player
                && table[3][1] == player) {
            return true;
        }
        return false;
    }

    public static boolean checkWin() {
        boolean check = false;
        checkCol();
        checkRow();
        checkCross();
        if (checkCol() == true || checkRow() == true || checkCross() == true) {
            System.out.println("Player" + " " + player + " " + "Win....");
            System.out.println("Bye Bye ... ");
            return true;
        }
        return false;
    }

}
